<?php namespace Enyodev\Utils\Storage;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Str;

/**
 * Class representing a service for storing files associated with
 * entities implementing AttachableInterface.
 */
class StorageService
{
    /**
     * Instance of filesystem to be used to store files.
     *
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Length of the random names to generate.
     *
     * @var int
     */
    private $name_size;

    /**
     * Length of the sub-folder names to create.
     *
     * @var int
     */
    private $chunk_size;

    /**
     * Number of sub folder to create.
     *
     * @var int
     */
    private $depth;

    /**
     * Create a FileStorage instance by injecting the filesystem
     * instance and the storage parameters.
     */
    public function __construct(Filesystem $filesystem, $name_size = 32, $chunk_size = 2, $depth = 3)
    {
        if ($name_size < $chunk_size * $depth) {

            throw new WrongStorageParametersException('The name_size value must be greater than chunk_size * depth.');

        }

        $this->filesystem = $filesystem;
        $this->name_size = $name_size;
        $this->chunk_size = $chunk_size;
        $this->depth = $depth;
    }

    /**
     * Store the file attached to an attachable instance.
     *
     * @param   AttachableInteface $attachable the attachable instance
     * @return  AttachableInterface|boolean
     */
    public function store(AttachableInterface $attachable)
    {
        $file = $attachable->getAttachedFile();

        if (!$file) throw new AttachedFileNotSetException();

        $base_name = $attachable->getFileBaseName() ?: $this->getRandomName();
        $ext = $file->getClientOriginalExtension();
        $name = $base_name . '.' . $ext;
        $path = $this->getPath($name);
        $contents = file_get_contents($file);

        $write_ok = $this->filesystem->put($path, $contents);

        if ($write_ok) {

            $attachable->setFileBaseName($base_name);
            $attachable->setFileExt($ext);

            return $attachable;

        }

        return false;
    }

    /**
     * Remove the file attached to an attachable instance from the store.
     *
     * @param   AttachableInteface $attachable the attachable instance
     * @return  boolean
     */
    public function unstore(AttachableInterface $attachable)
    {
        $path = $this->getFilePath($attachable);

        return $this->filesystem->delete($path);
    }

    /**
     * Return the content of the file attached to an attachable instance.
     *
     * @param   AttachableInteface $attachable the attachable instance
     * @return  string
     */
    public function getFileContents(AttachableInterface $attachable)
    {
        $path = $this->getFilePath($attachable);

        return $this->filesystem->get($path);
    }

    /**
     * Return the absolute path of the file attached to an attachable instance.
     *
     * @param   AttachableInteface $attachable the attachable instance
     * @return  string
     */
    public function getAbsoluteFilePath(AttachableInterface $attachable)
    {
        $path_prefix = $this->filesystem->getDriver()->getAdapter()->getPathPrefix();

        return $path_prefix . $this->getFilePath($attachable);
    }

    /**
     * Return the path of the file attached to an attachable instance.
     *
     * @param   AttachableInteface $attachable the attachable instance
     * @return  string
     */
    public function getFilePath(AttachableInterface $attachable)
    {
        $base_name = $attachable->getFileBaseName();
        $ext = $attachable->getFileExt();

        $name = $base_name . '.' . $ext;

        return $this->getPath($name);
    }

    /**
     * Return the path of a file from its name. Prepends the sub-folders.
     *
     * @param   string $name the name of the file
     * @return  string
     */
    private function getPath($name)
    {
        $chunks = str_split($name, $this->chunk_size);

        $chunks = array_slice($chunks, 0, $this->depth);

        return implode(DIRECTORY_SEPARATOR, array_merge($chunks, [$name]));
    }

    /**
     * Generate a lowercased random name of specified length using laravel helpers.
     *
     * @return string
     */
    private function getRandomName()
    {
        return Str::lower(Str::random($this->name_size));
    }

    /**
     * Delete a file based on its path.
     *
     * @param   string $path the path of the file to be deleted
     * @return  boolean
     */
    private function delete($path)
    {
        return $this->filesystem->delete($path);
    }

}
