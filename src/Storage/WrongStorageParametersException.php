<?php namespace Enyodev\Utils\Storage;

/**
 * Exception thrown when storage parameters are inconsistent.
 */
class WrongStorageParametersException extends \Exception{}
