<?php namespace Enyodev\Utils\Storage;

/**
 * Exception thrown when trying store an attached file which hasn't been set.
 */
class AttachedFileNotSetException extends \Exception{}
