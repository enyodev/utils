<?php namespace Enyodev\Utils\Storage;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface representing entities which can be associated to a file.
 */
interface AttachableInterface
{
    /**
     * Associate the attached file to the entity.
     *
     * @param   UploadedFile $file the uploaded file to be associated to the entity.
     * @return  void
     */
    public function setAttachedFile(UploadedFile $file);

    /**
     * Return the file attached to the entity
     *
     * @return UploadedFile
     */
    public function getAttachedFile();

    /**
     * Set the base name of the file (without extension).
     *
     * @param   string $file_base_name the base name of the attached file
     * @return  void
     */
    public function setFileBaseName($file_base_name);

    /**
     * Return the base name of the attached file.
     *
     * @return string
     */
    public function getFileBaseName();

    /**
     * Set the extension of the file.
     *
     * @param   string $file_ext the extension of the attached file
     * @return  void
     */
    public function setFileExt($file_ext);

    /**
     * Return the extension of the attached file.
     *
     * @return string
     */
    public function getFileExt();

}
