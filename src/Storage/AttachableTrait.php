<?php namespace Enyodev\Utils\Storage;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Default implementation of AttachableInterface.
 */
trait AttachableTrait
{
    /**
     * Contains the file attached to the entity.
     *
     * @var UploadedFile
     */
    private $file;

    /**
     * Associate the attached file to the entity.
     *
     * @param   UploadedFile $file the uploaded file to be associated to the entity.
     * @return  void
     */
    public function setAttachedFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Return the file attached to the entity
     *
     * @return UploadedFile
     */
    public function getAttachedFile()
    {
        return $this->file;
    }

    /**
     * Set the base name of the file (without extension). Can be overrided if the
     * entity attribute name is different.
     *
     * @param   string $file_base_name the base name of the attached file
     * @return  void
     */
    public function setFileBaseName($file_base_name)
    {
        $this->file_base_name = $file_base_name;
    }

    /**
     * Return the base name of the attached file. Can be overrided if the
     * entity attribute name is different.
     *
     * @return string
     */
    public function getFileBaseName()
    {
        return $this->file_base_name;
    }

    /**
     * Set the extension of the file. Can be overrided if the entity attribute
     * name is different.
     *
     * @param   string $file_ext the extension of the attached file
     * @return  void
     */
    public function setFileExt($file_ext)
    {
        $this->file_ext = $file_ext;
    }

    /**
     * Return the extension of the attached file. Can be overrided if the entity
     * attribute name is different.
     *
     * @return string
     */
    public function getFileExt()
    {
        return $this->file_ext;
    }

    /**
     * Define an accessor returning the whole name (base name + extension).
     *
     * @return string
     */
    public function getFileNameAttribute()
    {
        return $this->file_base_name . '.' . $this->file_ext;
    }

}
