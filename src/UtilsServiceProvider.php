<?php namespace Enyodev\Utils;

use Enyodev\Utils\Responses\RenderableInterface;
use Enyodev\Utils\Responses\DownloadableInterface;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;

/**
 * Service provider registering services for enyodev/utils.
 */
class UtilsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->call([$this, 'registerResponseMacros']);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register additional response macro.
     *
     * @return void
     */
    public function registerResponseMacros(Request $request, ResponseFactory $response)
    {
        $response->macro('format', function ($format, $resource, $default = null) use ($request, $response) {

            if ($format == '.json' or $request->wantsJson()) {

                return $response->json($resource);

            }

            if ($format) {

                return $response->file($resource);

            }

            return $default;

        });

        $response->macro('render', function (RenderableInterface $renderable) {

            $renderable->render();

        });

        $response->macro('file', function (DownloadableInterface $downloadable) use($response) {

            $file_path = $downloadable->getDownloadFilePath();
            $file_name = $downloadable->getDownloadFileName();
            $file_headers = $downloadable->getDownloadFileHeaders();

            return $response->download($file_path, $file_name, $file_headers);

        });
    }

}
