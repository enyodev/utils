<?php namespace Enyodev\Utils\Format;

use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Serializer\DataArraySerializer;
use Illuminate\Database\Eloquent\Model;

/**
 * Default implementation of TransformableCollectionInterface.
 */
trait TransformableCollectionTrait
{
    use TransformableTrait;

    /**
     * Return either the specified custom serializer or the default serializer.
     *
     * @return League\Fractal\Serializer\SerializerAbstract
     */
    public function getSerializer()
    {
        return $this->serializer ?: new DataArraySerializer;
    }

    /**
     * Return either the specified custom transformer or the default transformer.
     *
     * @return League\Fractal\Serializer\TransformerAbstract|callable
     */
    public function getTransformer()
    {
        return $this->transformer ?: function (Model $model) {

            return $model->toArray();

        };
    }

    /**
     * Override the Eloquent toArray method by returning the transformed data
     * as array using fractal.
     *
     * @return array
     */
    public function toArray()
    {
        $manager = new Manager;

        $manager->setSerializer($this->getSerializer());

        $resource = new Fractal\Resource\Collection($this, $this->getTransformer());

        return $manager->createData($resource)->toArray();
    }

}
