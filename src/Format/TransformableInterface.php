<?php namespace Enyodev\Utils\Format;

use League\Fractal\TransformerAbstract;
use League\Fractal\Serializer\SerializerAbstract;

/**
 * Interface representing an object which can be transformed via fractal.
 */
interface TransformableInterface
{
    /**
     * Set a custom serializer and return this instance for chaining.
     *
     * @param   SerializerAbstract $serializer the serializer to be user for the transformation
     * @return  mixed
     */
    public function setSerializer(SerializerAbstract $serializer);

    /**
     * Set a custom transformer and return this instance for chaining.
     *
     * @param   TransformerAbstract $transformer the transformer to be user for the transformation
     * @return  mixed
     */
    public function setTransformer(TransformerAbstract $transformer);

}
