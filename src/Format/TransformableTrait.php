<?php namespace Enyodev\Utils\Format;

use League\Fractal\Serializer\SerializerAbstract;
use League\Fractal\TransformerAbstract;

trait TransformableTrait
{
    private $serializer;
    private $transformer;

    /**
     * Set a custom serializer and return this instance for chaining.
     *
     * @return mixed
     */
    public function setSerializer(SerializerAbstract $serializer)
    {
        $this->serializer = $serializer;

        return $this;
    }

    /**
     * Set a custom transformer and return this instance for chaining.
     *
     * @return mixed
     */
    public function setTransformer(TransformerAbstract $transformer)
    {
        $this->transformer = $transformer;

        return $this;
    }

}
