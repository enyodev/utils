<?php namespace Enyodev\Utils\Format\Serializers;

use League\Fractal\Serializer\ArraySerializer;

/**
 * Default serializer to be used when no serializer is specified.
 */
class DefaultSerializer extends ArraySerializer
{
    /**
     * For a collection, the data are wrapped under a 'data' key if
     * no resource key is specified. If a resource key is specified
     * the data are not wrapped. Useful for not wrapping the includes.
     *
     * @param   string  $resourceKey    the specified resource key
     * @param   array   $data           the data to serialize
     * @return  array
     */
    public function collection($resourceKey, array $data)
    {
        return ($resourceKey) ? $data : ['data' => $data];
    }

}
