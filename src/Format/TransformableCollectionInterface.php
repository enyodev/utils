<?php namespace Enyodev\Utils\Format;

/**
 * Interface representing a collection which can be transformed via fractal.
 */
interface TransformableCollectionInterface extends TransformableInterface{}
