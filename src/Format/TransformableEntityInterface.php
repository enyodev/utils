<?php namespace Enyodev\Utils\Format;

/**
 * Interface representing an entity which can be transformed via fractal.
 */
interface TransformableEntityInterface extends TransformableInterface{}
