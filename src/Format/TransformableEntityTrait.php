<?php namespace Enyodev\Utils\Format;

use Enyodev\Utils\Format\Serializers\DefaultSerializer;
use League\Fractal;
use League\Fractal\Manager;
use Illuminate\Database\Eloquent\Model;

/**
 * Default implementation of TransformableEntityInterface.
 */
trait TransformableEntityTrait
{
    use TransformableTrait;

    /**
     * Return either the specified custom serializer or the default serializer.
     *
     * @return League\Fractal\Serializer\SerializerAbstract
     */
    public function getSerializer()
    {
        return $this->serializer ?: new DefaultSerializer;
    }

    /**
     * Return either the specified custom transformer or the default transformer.
     *
     * @return League\Fractal\Serializer\TransformerAbstract|callable
     */
    public function getTransformer()
    {
        return $this->transformer ?: function (Model $model) {

            return $model->toArrayRaw();

        };
    }

    /**
     * Return the original array returned by the Eloquent toArray method.
     *
     * @return array
     */
    public function toArrayRaw()
    {
        return parent::toArray();
    }

    /**
     * Override the Eloquent toArray method by returning the transformed data
     * as array using fractal.
     *
     * @return array
     */
    public function toArray()
    {
        $manager = new Manager;

        $manager->setSerializer($this->getSerializer());

        $resource = new Fractal\Resource\Item($this, $this->getTransformer());

        return $manager->createData($resource)->toArray();
    }

}
