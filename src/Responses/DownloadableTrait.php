<?php namespace Enyodev\Utils\Responses;

/**
 * Default implementation of the DownloadableInterface.
 */
trait DownloadableTrait
{
    /**
     * Return the headers to be given to the downloaded file. By default return
     * an empty array for the headers but can be overrided by child classes.
     *
     * @return array
     */
    public function getDownloadFileHeaders()
    {
        return [];
    }

}
