<?php namespace Enyodev\Utils\Responses;

interface RenderableInterface
{
    public function render();

}
