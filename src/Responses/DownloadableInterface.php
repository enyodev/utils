<?php namespace Enyodev\Utils\Responses;

/**
 * Interface representing entities which can be downloaded.
 */
interface DownloadableInterface
{
    /**
     * Return the downloaded path of the file.
     *
     * @return string
     */
    public function getDownloadFilePath();

    /**
     * Return the name to be given to the downloaded file.
     *
     * @return string
     */
    public function getDownloadFileName();

    /**
     * Return the headers to be given to the downloaded file.
     *
     * @return string
     */
    public function getDownloadFileHeaders();

}
