<?php namespace Enyodev\Utils\Csv;

/**
 * Interface representing a collection which can be represented as csv.
 */
interface CsvableCollectionInterface extends CsvableInterface{}
