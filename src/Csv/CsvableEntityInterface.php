<?php namespace Enyodev\Utils\Csv;

/**
 * Interface representing an entity which can be represented as csv.
 */
interface CsvableEntityInterface extends CsvableInterface{}
