<?php namespace Enyodev\Utils\Csv;

/**
 * Default implementation of CsvableEntityInterface.
 */
trait CsvableEntityTrait
{
  use CsvableTrait;

  // If someday there is some default behavior to implements the trait is already there :)

}
