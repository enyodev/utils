<?php namespace Enyodev\Utils\Csv;

use Enyodev\Utils\Responses\DownloadableInterface;

/**
 * Interface representing an object which can be represented as csv.
 */
interface CsvableInterface extends DownloadableInterface
{
    /**
     * Return the name of the csv file representing the object.
     *
     * @return string
     */
    public function getCsvFileName();

    /**
     * Return the path of the csv file representing the object.
     *
     * @return string
     */
    public function getCsvFilePath();

    /**
     * Return the matrix of cells of the csv file representing the object.
     *
     * @return array
     */
    public function getCsvCells();

}
