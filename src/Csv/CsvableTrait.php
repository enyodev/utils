<?php namespace Enyodev\Utils\Csv;

/**
 * Default implementation of csvableInterface.
 */
trait CsvableTrait
{
    /**
     * Return the name of the csv file representing the object. If there
     * is a csv_file_name attribute defined on the child class, return it,
     * return a default name otherwise. Can be overrided by child classes.
     *
     * @return string
     */
    public function getCsvFileName()
    {
        return isset($this->csv_file_name)
            ? $this->csv_file_name
            : 'file.csv';
    }

    /**
     * Return the path of the csv file representing the object. Save the csv
     * data into a temporary file and return its path. Can be overrided by
     * the child classes.
     *
     * @return string
     */
    public function getCsvFilePath()
    {
        $csv_file_path = tempnam(sys_get_temp_dir(), 'csv_');

        $this->saveCsvFile($csv_file_path);

        return $csv_file_path;
    }

    /**
     * Return the contents of the csv file representing the object based on the
     * getCsvCells method.
     *
     * @return string
     */
    public function getCsvFileContents()
    {
        $csv_quote = '"';
        $csv_delimiter = ';';

        $cells = $this->getCsvCells();

        $contents = '';

        foreach ($cells as $line) {

            $formatted_cells = array_map(function ($cell) use ($csv_quote) {

                return $csv_quote . $cell . $csv_quote;

            }, $line);

            $contents.= implode($csv_delimiter, $formatted_cells) . "\n";

        }

        return $contents;
    }

    /**
     * Return the contents of the csv file representing the object into a file.
     *
     * @return mixed
     */
    public function saveCsvFile($csv_file_path)
    {
        $contents = $this->getCsvFileContents();

        return file_put_contents($csv_file_path, $contents);
    }

    /**
     * Return the path of the csv file representing the object as the path of
     * the downloadable file.
     *
     * @return string
     */
    public function getDownloadFilePath()
    {
        return $this->getCsvFilePath();
    }

    /**
     * Return the name of the csv file representing the object as the name of
     * the downloadable file.
     *
     * @return string
     */
    public function getDownloadFileName()
    {
        return $this->getCsvFileName();
    }

    /**
     * Return headers for a csv file as the headers of the downloadable file.
     *
     * @return array
     */
    public function getDownloadFileHeaders()
    {
        return ['Content-type' => 'text/csv'];
    }

}
