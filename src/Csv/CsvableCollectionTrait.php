<?php namespace Enyodev\Utils\Csv;

/**
 * Default implementation of CsvableCollectionInterface.
 */
trait CsvableCollectionTrait
{
    use CsvableTrait;

    /**
     * Return the sorted collection. No sort applied by default but can be
     * overrided by child classes.
     *
     * @return Illuminate\Support\Collection
     */
    public function getSortedCsvLines()
    {
        return $this;
    }

    /**
     * Default method for returning a matrix representing the collection as csv cells.
     * Expect a getCsvLine method to be implemented but can be overrided by child classes.
     *
     * @return array
     */
    public function getCsvCells()
    {
        $cells = [];

        if (isset($this->csv_header)) $cells[] = $this->csv_header;

        foreach ($this->getSortedCsvLines() as $model) {

            $cells[] = $this->getCsvLine($model);

        }

        return $cells;
    }

}
